# Template for new LaTeX papers

## Setup

* Rename `src/rename-me.tex` to `src/your-project-name.tex`.
* In `Makefile`, replace all occurrences of `rename-me` with `your-project-name`.

## Usage

* `make` (or `make quick`): Build `your-project-name.pdf` quickly but without references nor citations.
* `make bib`: Build `your-project-name.pdf` with full references and citations. Takes at least triple the time of `make`.
* `make figures`: Build tikz figures from dot files. Creates `src/figures/auto-tikz/*`.
* `make clean`: Remove all built files, including any PDFs.
* `make your-project-name.pdf`: Shortcut to clean and build full PDF in one go. Useful if in an inconsistent state (e.g., PDF is missing but placeholder files (`quick`/`bib`) are present).
