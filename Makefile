TEXFILES := $(shell find src/ -print)
TIKZS = $(patsubst src/figures/dot/%.dot, src/figures/auto-tikz/%.tex, $(wildcard src/figures/dot/*.dot))

quick: $(TEXFILES)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode rename-me.tex; \
	touch quick; \
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib

bib: $(TIKZS) $(TEXFILES)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode rename-me.tex; \
	bibtex rename-me.aux; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode rename-me.tex; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode rename-me.tex; \
	touch quick bib; \
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib

rename-me.pdf: clean bib

figures: $(TIKZS)

src/figures/manual-tikz/*:

src/figures/auto-tikz/%.tex: src/figures/dot/%.dot
	mkdir -p src/figures/auto-tikz/
	dot2tex --texmode math --format tikz --figonly --autosize --usepdflatex --nominsize --prog dot $< > $@

clean:
	rm -rf *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib *.pdf quick bib src/figures/auto-tikz/
